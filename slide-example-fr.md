---
type: slide
slideOptions:
  transition: slide
---

# Exemple de Diaporama

Choisissez Menu / Mode Présentation
puis utilisez la touche `espace`

Vous pouvez utiliser `slideOptions` en en-tête pour personnaliser vos diaporamas.

---

## Première diapo

`---`

est le séparateur de diapos

----

### Première branche de la première diapo

`----`

est le séparateur de branches

Utiliser la _touche espace_ pour naviguer entre les diapos.

----

### Seconde branche de la première diapo

Les diapositives imbriquées sont utiles pour ajouter des détails supplémentaires sous une diapositive principale.

---

## Point de vue général

Appuyez sur **ESC** pour entrer dans le mode de visualisation global.

---

## Optimisé pour écran tactile

Les diaporamas sont faciles d'usage sur tablettes, smartphones ou écran tactiles.
Faites glisser votre doigt pour naviguer entre les diapos.

---

## Fragments

`<!-- .element: class="fragment" data-fragment-index="1" -->`

La syntaxe des fragments : syntax

Appuyez sur la flèche Droite...

... pour passer à l'étape suivante ...

<span>... une<!-- .element: class="fragment" data-fragment-index="1" --></span> <span>diapositive<!-- .element: class="fragment" data-fragment-index="2" --></span> <span>fragmentée.<!-- .element: class="fragment" data-fragment-index="3" --></span>

Note:
  Cette diapo possède des fragments qui sont aussi décomposées dans la fenêtre des Notes.

---

## Style de fragments

Il y a plusieurs types de fragments (en anglais), comme :
* grow
* shrink
* fade-out
* fade-up (aussi vers le bas, la gauche et la droite!)
* current-visible

Mise en évidence <span><!-- .element: class="fragment highlight-red" -->rouge</span> <span><!-- .element: class="fragment highlight-blue" -->bleue</span> <span><!-- .element: class="fragment highlight-green"-->verte</span>

---

<!-- .slide: data-transition="zoom" -->

## Styles de transition
Différents type de transitions sont disponibles via des options. Celle-ci s'appelle "zoom".

`<!-- .slide: data-transition="zoom" -->`

est la syntaxe de transition

Vous pouvez aussi utiliser (en anglais uniquement) :

none/fade/slide/convex/concave/zoom

---

<!-- .slide: data-transition="fade-in convex-out" -->

`<!-- .slide: data-transition="fade-in convex-out" -->`

Cepdendant, vous pouvez aussi définir différentes transitions d'entrée et de sorties

Vous pouvez utiliser (en anglais uniquement):

none/fade/slide/convex/concave/zoom

avec le suffixe `-in` ou `-out`

---

<!-- .slide: data-transition-speed="fast" -->

`<!-- .slide: data-transition-speed="fast" -->`

Vous pouvez aussi personnaliser la vitesse de transition !

Vous pouvez utiliser (en anglais uniquement) :

default/fast/slow

---

## Thèmes - Apparence

Les diaporamas supportent plusieurs thèmes internes comme :

Black (default) - White - League - Sky - Beige - Simple

Serif - Blood - Night - Moon - Solarized

Ils peuvent être définis dans les options d'en-tête avec slideOptions

---

<!-- .slide: data-background="#1A237E" -->

`<!-- .slide: data-background="#1A237E" -->`

est la syntaxe de couleur de fond d'écran

---

<!-- .slide: data-background="/images/fond.jpg" data-background-color="#005" -->

<div style="color: #fff;">

## Image d'arrière plan

`<!-- .slide: data-background="adresse_image.png"-->`

</div>

----

<!-- .slide: data-background="/images/logo-cemea.jpg" data-background-repeat="repeat" data-background-size="100px" data-background-color="#005" -->

<div style="color: #fff;">

## Image de fond d'écran répétés

`<!-- .slide: data-background="adresse_image.jpg" data-background-repeat="repeat" data-background-size="100px" -->`

</div>

----

<!-- .slide: data-background-video="https://s3.amazonaws.com/static.slid.es/site/homepage/v1/homepage-video-editor.mp4,https://s3.amazonaws.com/static.slid.es/site/homepage/v1/homepage-video-editor.webm" data-background-color="#000000" -->

<div style="background-color: rgba(0, 0, 0, 0.9); color: #fff; padding: 20px;">

## Vidéo en arrière plan

`<!-- .slide: data-background-video="video.mp4,video.webm" -->`

</div>

----

<!-- .slide: data-background="http://i.giphy.com/90F8aUepslB84.gif" -->

## ... et des mêmes !

---

## Présentation de code

``` javascript
function linkify( selector ) {
  if( supports3DTransforms ) {

    const nodes = document.querySelectorAll( selector );

    for( const i = 0, len = nodes.length; i < len; i++ ) {
      var node = nodes[i];

      if( !node.className ) {
        node.className += ' roll';
      }
    }
  }
}
```
La coloration syntaxique du code est utilisée à partir de [highlight.js](http://softwaremaniacs.org/soft/highlight/en/description/).

---

## des listes à puce (merveilleuses)

- Pas d'ordre ici
- là non plus
- non plus
- là encore moins

---

## Ou des listes ordonnées (fantastiques)

1. Le premier se trouve avant...
2. le second, suivi...
3. du troisième !

---

## des tableaux

| Elément    | Valeur | Quantité |
| ----     | ----- | -------- |
| Pommes   | 1 €    | 7        |
| Limonade | 2 €    | 18       |
| Pain    | 3 €   | 2        |

---

## Citations

> “Depuis des années, il existe une théorie selon laquelle des millions de singes tapant au hasard sur des millions de machines à écrire reproduiraient l'intégralité des œuvres de Shakespeare. L'Internet a prouvé que cette théorie était fausse.”

---

## Liens internes

Vous pouvez relier les diapositives entre elles, [comme celle-ci](#/1/3).

---

## Présentateur

Il y a un [mode présentateur](https://github.com/hakimel/reveal.js#speaker-notes). Il inclut un timer, une prévisualisation de la diapo à venir ainsi que vos notes de présentateur.

Appuyez sur la touche _S_ key pour l'essayer.

Note:
  Hé, il y a des notes par ici. Elles sont cachées pendant la présentation, mais vous pouvez les voir si vous ouvrez la fenêtre des notes du présentateur (appuyez sur la touche `s` de votre clavier)

---

## Faites une pause

Appuyez sur `B` ou `.` sur votre clavier pour faire une pause de votre présentation. Ceci est utile en formation, lorsque vous souhaitez cacher votre écran.

---

## Imprimez vos diapos

Descendez-ci dessous et vous verrez l'icone d'impression <i class="fa fa-fw fa-print"></i>.

Après avoir cliqué dessus, utilisez la fonction "Imprimer" de votre navigateur (comme CTRL+P or cmd+P) pour imprimer vos dispositives en tant que PDF. [Voir la documentation officielle de reveal.js pour plus de détails](https://github.com/hakimel/reveal.js#instructions-1)

---

# Fin !

